package com.recoveryportal.constants.core;
/**
 * The Class ApplicationConstants.
 */
public class ApplicationConstants {
	public static final String ACCESS_TOKEN = "accessToken";

	/**
	 * The Constant ACTIVE.
	 */
	public static final String ACTIVE = "isActive";

	public static final String ADMIN = "admin";

	public static final String ALL = "ALL";

	public static final String APPLICATION_VND_MS_EXCEL = "application/vnd.ms-excel";

	public static final String ATTACHEMENT_FILENAME = "attachment; filename=";

	public static final String ATTACHMENT_SLASH = "\"";

	public static final String AUTH = "auth";

	public static final String AUTHORIZATION = "Authorization";

	/**
	 * The Bad Request constant
	 */
	public static final String BAD_REQUEST = "Bad Request";

	public static final String BASIC = "Basic";

	public static final String BEARER = "Bearer ";

	/**
	 * The Constant BETWEEN.
	 */
	public static final String BETWEEN = "BW";

	public static final String BUSINESS_UNIT_NOT_FOUND = "Business Unit not found";

	/**
	 * The Constant CHECK_SERVER_STATUS.
	 */
	public static final String CHECK_SERVER_STATUS = "CHECK_SERVER_STATUS";

	/**
	 * The Constant CHECK_SERVER_STATUS_URL.
	 */
	public static final String CHECK_SERVER_STATUS_URL = "/app/server/status";

	public static final String CLOSE_BRACE = ")";

	public static final String COLON = ":";
	/**
	 * The Constant COLON_REGEX.
	 */
	public static final String COLON_REGEX = "\\:";

	public static final String COMMA = ",";

	public static final String CSV = "text/csv";
	/**
	 * The Role blocked success constant
	 */
	public static final String DEFAULT_ERROR_MSG = "User Already Exists";

	/**
	 * Default expiration time
	 */
	public static final int DEFAULT_EXPIRATION_TIME = 10;

	/**
	 * The Constant DEFAULT_FULL_DATE_FORMAT.
	 */
	public static final String DEFAULT_FULL_DATE_FORMAT = "MM/dd/yyyy HH:mm";

	public static final String DEPRECATION = "deprecation";

	/**
	 * The Constant DOT_REGEX.
	 */
	public static final String DOT_REGEX = "\\.";

	public static final String EMAIL_AT = "@";

	public static final String EMPTY_FILE = "Empty file";

	/**
	 * The Constant EMPTY_STRING.
	 */
	public static final String EMPTY_STRING = " ";

	/**
	 * The Constant EQUAL.
	 */
	public static final String EQUAL = "Equal";

	public static final String FAILED_TO_SAVE_RECORD = "failed to save record for : {} th row";

	public static final String FILE_TYPE = "text/csv";

	/**
	 * The Constant FILTER.
	 */
	public static final String FILTER = "FILTER";

	/**
	 * The Constant FILTER_TYPE_BOOLEAN.
	 */
	public static final String FILTER_TYPE_BOOLEAN = "Boolean";

	/**
	 * The Constant FILTER_TYPE_DATE.
	 */
	public static final String FILTER_TYPE_DATE = "Date";

	/**
	 * The Constant FILTER_TYPE_DOUBLE.
	 */
	public static final String FILTER_TYPE_DOUBLE = "Double";

	/**
	 * The Constant FILTER_TYPE_INTEGER.
	 */
	// ******* for common filter constants start
	public static final String FILTER_TYPE_INTEGER = "Integer";

	/**
	 * The Constant FILTER_TYPE_TEXT.
	 */
	public static final String FILTER_TYPE_TEXT = "String";
	public static final String FORBIDDEN = "Forbidden";

	/**
	 * The Constant FORWARDED_FOR.
	 */
	public static final String FORWARDED_FOR = "X-Forwarded-For";
	/**
	 * The Constant FORWARDED_HOST.
	 */
	public static final String FORWARDED_HOST = "X-Forwarded-Host";

	/**
	 * The Constant GT.
	 */
	public static final String GT = "Gt";

	/**
	 * The Constant GTE.
	 */
	public static final String GTE = "Ge";

	/**
	 * The Constant IDENTITY.
	 */
	public static final String IDENTITY = "Identity";

	/**
	 * The Constant IDENTITY_CONSTANT.
	 */
	public static final String IDENTITY_CONSTANT = "identity";

	public static final String INACTIVE = "InActive";

	public static final String INSTITUTION_ID = "InstitutionId";

	public static final String INVALID_DATA = "INVALID_DATA";

	public static final String INVALID_USER = "invalid user";
	/**
	 * The Constant IS_ACTIVE.
	 */
	public static final String IS_ACTIVE = "isActive";

	public static final String IS_AUTHENTICATED_FULLY = "IS_AUTHENTICATED_FULLY";

	/**
	 * The Constant IS_DELETED.
	 */
	public static final String IS_DELETED = "isDeleted";

	public static final String IS_NOT_AUTHENTICATED_FULLY = "IS_NOT_AUTHENTICATED_FULLY";

	public static final String IS_TWO_FA_AUTHENTICATED = "isTwoFAAuthenticated";

	/**
	 * The Constant LIKE.
	 */
	public static final String LIKE = "Like";

	public static final String LIMIT = "limit";

	/**
	 * The Constant LT.
	 */
	public static final String LT = "Lt";

	/**
	 * The Constant LTE.
	 */
	public static final String LTE = "Le";

	public static final int MAX_ERROR_LIMIT = 230;

	/**
	 * The Constant MINUS_ONE.
	 */
	public static final String MINUS_ONE = "-1";

	public static final String NEW_ROLE = "new_role";

	/**
	 * The Constant ONE.
	 */
	public static final int ONE = 1;

	public static final String OPEN_BRACE = "(";

	/**
	 * The Constant ORDERBY.
	 */
	public static final String ORDERBY = "orderBy";
	/**
	 * Default expiration time
	 */
	public static final int PASSWORD_HISTORY_LIMIT = 4;

	// Don't change this constant
	public static final String PWD_EXPIRED_ERROR = "Password expired exception error message";

	public static final String RECORD = "record : ";

	public static final String REST_AUTHENTICATION_ENTRY_POINT = "restAuthenticationEntryPoint";

	public static final String RLE_ACTIVE = "Active";

	public static final String ROLE = "role";

	public static final String ROLE_2FA = "Role2Fa";

	public static final String ROLE_2FA_GET_URL = "/otp/get-otp";

	public static final String ROLE_2FA_VERIFY_URL = "/otp/verify";

	public static final String ROLE_ADMIN = "ROLE_ADMIN";

	public static final String ROLE_NOT_FOUND = "Role not found";

	public static final String ROLE_PWD_EXPIRED_URL = "/user/force-update-password";
	/**
	 * The Role saved success constant
	 */
	public static final String ROLE_SAVED_SUCCESS = "Role Saved Successfully.";

	public static final String ROLE_SHORT_LIVING = "role-short-living";

	/**
	 * The Role updated success constant
	 */
	public static final String ROLE_UPDATED_SUCCESS = "Role Updated Successfully.";

	/**
	 * The Constant ROUNDOFF.
	 */
	public static final String ROUNDOFF = "ROUNDOFF";

	public static final String SET_IDENTITY = "setIdentity";

	/**
	 * The Constant SIX.
	 */
	public static final int SIX = 6;

	public static final String SKIP = "skip";

	/**
	 * The Constant SLASH.
	 */
	public static final String SLASH = "/";

	/**
	 * The Constant SORTING.
	 */
	public static final String SORTING = "SORTING";
	// for common filter constants end *******\

	public static final int SPLITTOKEN = 2;

	public static final String TEST = "test";

	/**
	 * The Constant TOO_MANY_REQUEST_FOUND.
	 */
	public static final String TOO_MANY_REQUEST_FOUND = "Too Many Request found for the input URL : ";

	public static final String UNAUTHORIZED = "Unauthorized";

	public static final String USER = "user";

	/**
	 * The Role blocked success constant
	 */
	public static final String USER_BLOCKED_SUCCESS = "User Blocked Successfully.";

	public static final String USER_DOMAIN_CHECK = "Invalid Domain";

	public static final String USER_LIMIT_CHECK = "User limit reached";

	/**
	 * The Role deleted success constant
	 */
	public static final String USER_NOT_FOUND = "User Not Found.";

	/**
	 * The Role blocked success constant
	 */
	public static final String USER_SAVED_SUCCESS = "User Saved Successfully.";

	/**
	 * The Role blocked success constant
	 */
	public static final String USER_UNBLOCKED_SUCCESS = "User UnBlocked Successfully.";

	/**
	 * The Constant SEMI_COLON.
	 */
	public static final String WITHOUT_SPACE = "";

	/**
	 * The Constant ZERO.
	 */
	public static final int ZERO = 0;

	private ApplicationConstants() {
		/**/}


}
