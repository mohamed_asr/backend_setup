package com.recoveryportal.constants;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecoveryportalConstantsDevelopApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecoveryportalConstantsDevelopApplication.class, args);
	}

}
