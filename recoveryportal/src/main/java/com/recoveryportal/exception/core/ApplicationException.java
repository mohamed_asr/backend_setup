package com.recoveryportal.exception.core;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.recoveryportal.exception.core.codes.ErrorId;
import com.recoveryportal.exception.core.codes.ErrorIdList;
/**
 * The Class ApplicationException.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ApplicationException extends Exception {
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 676852299677539062L;
	
	/**
	 * Gets the new.
	 *
	 * @param errorId the error id
	 * @return the new
	 */



	/**
	 * The error id list.
	 */
	private ErrorIdList errorIdList = new ErrorIdList();

	/**
	 * Instantiates a new application exception.
	 */
	public ApplicationException() {
		super();
	}

	/**
	 * Instantiates a new application exception.
	 *
	 * @param errorId the error id
	 */
	public ApplicationException(ErrorId errorId) {
		super(new ErrorIdList(errorId).convertToJsonString());
		errorIdList.addError(errorId);
	}

	/**
	 * Instantiates a new application exception.
	 *
	 * @param errorIdList the error id list
	 */
	public ApplicationException(ErrorIdList errorIdList) {
		super(errorIdList.convertToJsonString());
		this.errorIdList = errorIdList;
	}

	/**
	 * Instantiates a new application exception.
	 *
	 * @param message the message
	 */
	public ApplicationException(String message) {
		super(message);
	}

	/**
	 * Gets the error id.
	 *
	 * @param errorCode the error code
	 * @return the error id
	 */
	public ErrorId getErrorId(String errorCode) {
		return errorIdList.getErrorIds().stream().filter(e -> e.getErrorCode().equals(errorCode)).findFirst()
				.orElse(null);

	}

	/**
	 * Gets the stack trace.
	 *
	 * @return the stack trace
	 */
	@Override
	@JsonIgnore
	public StackTraceElement[] getStackTrace() {
		return super.getStackTrace();
	}
}

	
	
	
	

