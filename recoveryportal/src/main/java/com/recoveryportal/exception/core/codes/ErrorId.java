package com.recoveryportal.exception.core.codes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class ErrorId.
 */
public class ErrorId implements Serializable {
	/**
	 * The Class ErrorHint
	 */
	public static class ErrorHint implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * The hint key.
		 */
		private String hintKey;

		/**
		 * The hint name.
		 */
		private String hintName;

		/**
		 * The hint type.
		 */
		private String hintType = HintType.TEXT.name();

		/**
		 * The hint value.
		 */
		private String hintValue;

		/**
		 * Instantiates a new error hint.
		 */
		public ErrorHint() {

		}

		/**
		 * Instantiates a new error hint.
		 *
		 * @param hintKey   the hint key
		 * @param hintValue the hint value
		 */
		public ErrorHint(String hintKey, String hintValue) {
			this.hintKey = hintKey;
			this.hintValue = hintValue;
		}

		/**
		 * Instantiates a new error hint.
		 *
		 * @param hintKey   the hint key
		 * @param hintValue the hint value
		 * @param hintType  the hint type
		 */
		public ErrorHint(String hintKey, String hintValue, String hintType) {
			this.hintKey = hintKey;
			this.hintValue = hintValue;
			this.hintType = hintType;
		}

		/**
		 * Instantiates a new error hint.
		 *
		 * @param hintKey   the hint key
		 * @param hintValue the hint value
		 * @param hintType  the hint type
		 * @param hintName  the hint name
		 */
		public ErrorHint(String hintKey, String hintValue, String hintType, String hintName) {
			this.hintKey = hintKey;
			this.hintValue = hintValue;
			this.hintType = hintType;
			this.hintName = hintName;
		}

		/**
		 * Gets the hint key.
		 *
		 * @return the hint key
		 */
		public String getHintKey() {
			return hintKey;
		}

		/**
		 * Gets the hint name.
		 *
		 * @return the hint name
		 */
		public String getHintName() {
			return hintName;
		}

		/**
		 * Gets the hint type.
		 *
		 * @return the hint type
		 */
		public String getHintType() {
			return hintType;
		}

		/**
		 * Gets the hint value.
		 *
		 * @return the hint value
		 */
		public String getHintValue() {
			return hintValue;
		}

		/**
		 * Sets the hint key.
		 *
		 * @param hintKey the new hint key
		 */
		public void setHintKey(String hintKey) {
			this.hintKey = hintKey;
		}

		/**
		 * Sets the hint name.
		 *
		 * @param hintName the new hint name
		 */
		public void setHintName(String hintName) {
			this.hintName = hintName;
		}

		/**
		 * Sets the hint type.
		 *
		 * @param hintType the new hint type
		 */
		public void setHintType(String hintType) {
			this.hintType = hintType;
		}

		/**
		 * Sets the hint value.
		 *
		 * @param hintValue the new hint value
		 */
		public void setHintValue(String hintValue) {
			this.hintValue = hintValue;
		}
	}

	/**
	 * The Enum HintType.
	 */
	public enum HintType {

		/**
		 * The link.
		 */
		LINK,
		/**
		 * The text.
		 */
		TEXT
	}

	/**
	 * The Enum Severity.
	 */
	public enum Severity {

		/**
		 * The error.
		 */
		ERROR,
		/**
		 * The fatal.
		 */
		FATAL,
		/**
		 * The warning.
		 */
		WARNING
	}

	/**
	 * The hint type.
	 */
	private static final HintType hintType = HintType.TEXT;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Gets the new error.
	 *
	 * @param errorIdCode the error id code
	 * @return the new error
	 */
	public static ErrorId getNewError(ErrorId errorIdCode) {
		return new ErrorId(errorIdCode.getErrorCode(), errorIdCode.getErrorMessage());
	}

	/**
	 * The error code.
	 */
	private String errorCode;

	/**
	 * The error message.
	 */
	private String errorMessage;

	/**
	 * The hints.
	 */
	private List<ErrorHint> hints = new ArrayList<>();

	/**
	 * The severity.
	 */
	private Severity severity = Severity.ERROR;

	/**
	 * Instantiates a new error id.
	 */
	public ErrorId() {
	}

	/**
	 * Instantiates a new error id.
	 *
	 * @param errorCode    the error code
	 * @param errorMessage the error message
	 */
	public ErrorId(String errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	/**
	 * Instantiates a new error id.
	 *
	 * @param errorCode    the error code
	 * @param errorMessage the error message
	 * @param hints        the hints
	 */
	public ErrorId(String errorCode, String errorMessage, List<ErrorId.ErrorHint> hints) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.hints = hints;
	}

	/**
	 * Instantiates a new error id.
	 *
	 * @param errorCode    the error code
	 * @param errorMessage the error message
	 * @param severity     the severity
	 */
	public ErrorId(String errorCode, String errorMessage, Severity severity) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.severity = severity;
	}

	/**
	 * Instantiates a new error id.
	 *
	 * @param errorCode    the error code
	 * @param errorMessage the error message
	 * @param severity     the severity
	 * @param hints        the hints
	 */
	public ErrorId(String errorCode, String errorMessage, Severity severity, List<ErrorId.ErrorHint> hints) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.severity = severity;
		this.hints = hints;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Gets the hints.
	 *
	 * @return the hints
	 */
	public List<ErrorHint> getHints() {
		return hints;
	}

	/**
	 * Gets the hint type.
	 *
	 * @return the hint type
	 */
	public String getHintType() {
		return hintType.name();
	}

	/**
	 * Gets the severity.
	 *
	 * @return the severity
	 */
	public String getSeverity() {
		return severity.name();
	}

	/**
	 * Sets the hint.
	 *
	 * @param key   the key
	 * @param value the value
	 * @return the error id
	 */
	public ErrorId setHint(String key, String value) {
		this.hints.add(new ErrorHint(key, value));
		return this;
	}

	/**
	 * Sets the hint.
	 *
	 * @param key      the key
	 * @param value    the value
	 * @param hintType the hint type
	 * @return the error id
	 */
	public ErrorId setHint(String key, String value, String hintType) {
		this.hints.add(new ErrorHint(key, value, hintType));
		return this;
	}

	/**
	 * Sets the hint.
	 *
	 * @param key      the key
	 * @param value    the value
	 * @param hintType the hint type
	 * @param hintName the hint name
	 * @return the error id
	 */
	public ErrorId setHint(String key, String value, String hintType, String hintName) {
		this.hints.add(new ErrorHint(key, value, hintType, hintName));
		return this;
	}

	/**
	 * Sets the hints.
	 *
	 * @param hints the hints
	 * @return the error id
	 */
	public ErrorId setHints(List<ErrorHint> hints) {
		this.hints = hints;
		return this;
	}

	/**
	 * Sets the hints.
	 *
	 * @param key   the key
	 * @param value the value
	 * @return the error id
	 */
	public ErrorId setHints(String key, String value) {
		this.hints = new ArrayList<>();
		this.hints.add(new ErrorHint(key, value));
		return this;
	}
}
