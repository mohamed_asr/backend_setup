package com.recoveryportal.exception.core.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.recoveryportal.constants.core.ApplicationConstants;
import com.recoveryportal.exception.core.ApplicationException;
import com.recoveryportal.exception.core.applicationutils.ApplicationExceptionUtils;
import com.recoveryportal.exception.core.codes.ErrorCodes;
import com.recoveryportal.exception.core.codes.ErrorId;

/**
 * The Class ApplicationExceptionHandler.
 */
@ControllerAdvice
public class ApplicationExceptionHandler {
	
	/**
	 * The Constant logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(ApplicationExceptionHandler.class);

	/**
	 * Handle Application exception.
	 *
	 * @param ex the ex
	 * @return the application exception
	 */
	@ExceptionHandler(ApplicationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public static ApplicationException handleApplicationException(ApplicationException e) {
		logger.error(String.format("ApplicationException ========> %s", e.getMessage()));
		return e;
	}

	/**
	 * Handle exception.
	 *
	 * @param e the exception
	 * @return the application exception
	 */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public static ApplicationException handleException(Exception e) {
		logger.error(String.format("Exception ========> %s".concat(e.getMessage())));
		return new ApplicationException(ErrorCodes.INTERNAL_ERROR);
	}

	/**
	 * Handle message not readable exception.
	 *
	 * @param e the exception
	 * @return the application exception
	 */
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public static ApplicationException handleMessageNotReadableException(HttpMessageNotReadableException e) {
		logger.error(String.format("Exception ========> %s", e.getMessage()));

		if (ApplicationExceptionUtils.isValidateObject(e.getCause())
				&& ApplicationExceptionUtils.isNotBlank(e.getCause().getLocalizedMessage())
				&& e.getCause().getLocalizedMessage().contains(ApplicationConstants.TOO_MANY_REQUEST_FOUND)) {
			logger.error(String.format("Exception ========> %s", e.getCause().getLocalizedMessage()));
			return new ApplicationException(new ErrorId("E00522", e.getCause().getLocalizedMessage()));
		}
		e.printStackTrace();
		return new ApplicationException(ErrorCodes.INTERNAL_ERROR);
	}

	/**
	 *
	 * @param e the e
	 * @return the application exception
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public static ApplicationException handleTmsException(MethodArgumentNotValidException e) {

		String errMsg = e.getBindingResult().getFieldErrors().stream()
				.map(DefaultMessageSourceResolvable::getDefaultMessage).findFirst().orElse(e.getMessage());
		logger.error(String.format("Method Argument Exception ========> %s", errMsg));
		return new ApplicationException(new ErrorId("", errMsg));
	}

	private ApplicationExceptionHandler() {
		/**/}

}
