package com.recoveryportal.exception.core.codes;

/**
 * The Class ErrorCodes.
 */
public class ErrorCodes {
	
	public static final ErrorId BAD_REQUEST = new ErrorId("E0029", "Bad Request");

	public static final ErrorId COLUMN_ALREADY_EXISTS = new ErrorId("E0045", "Column Already Exists");

	public static final ErrorId DOMAIN_VALIDATION = new ErrorId("E0021", "Invalid Domain");

	public static final ErrorId DUPLICATE_FIELD_NAME = new ErrorId("E0047", "Field Name Already Exists.");

	public static final ErrorId DUPLICATE_OPTION_NAME = new ErrorId("E0052", "Option Name Already Exists...");

	public static final ErrorId DUPLICATE_VALUE = new ErrorId("E0032", "The field value already exists!");

	public static final ErrorId EMAIL_ALREADY_EXIST = new ErrorId("E0011", "Email Already Exists");

	public static final ErrorId EXISTING_USER = new ErrorId("E0028", "Existing user not found.");

	public static final ErrorId FIELD_VALUE_MANDATORY = new ErrorId("E0033",
			"Please fill the mandatory field(s) before saving.");

	public static final ErrorId FILE_EMPTY = new ErrorId("E0054", "The file is Empty");

	public static final ErrorId HISTORY_MATCHED = new ErrorId("E0017", "Old password matched, Try with new one");

	public static final ErrorId IDENTITY_VALUE_MISSING = new ErrorId("E0048", "Itendity Column value is missing");

	/**
	 * The internal error.
	 */
	public static final ErrorId INTERNAL_ERROR = new ErrorId("E0000", "Something went wrong. Please try again...");

	/**
	 * The invalid column name.
	 */
	public static final ErrorId INVALID_COLUMN_NAME = new ErrorId("E0003", "Invalid column name to filter..!");

	/**
	 * The invalid data.
	 */
	public static final ErrorId INVALID_DATA = new ErrorId("E0002",
			"Data type not valid. Please provide data with valid data type..!");

	public static final ErrorId INVALID_DATATYPE = new ErrorId("E0030",
			"Data type not valid. Please provide data with valid data type");

	/**
	 * The invalid user.
	 */
	public static final ErrorId INVALID_FILE = new ErrorId("E0006", "Invalid file");

	/**
	 * The invalid institution.
	 */
	public static final ErrorId INVALID_INSTITUTION = new ErrorId("E0027", "Invalid Institution");

	public static final ErrorId INVALID_KEY = new ErrorId("E0013", "Invalid url key");

	public static final ErrorId INVALID_LIST_ID = new ErrorId("E0035",
			"List Id not valid.Please provide valid list id");

	/**
	 * The invalid method name.
	 */
	public static final ErrorId INVALID_METHOD_NAME = new ErrorId("E0004", "Invalid Method name..!");

	public static final ErrorId INVALID_OBJECT = new ErrorId("E0039", "Invalid table details");

	public static final ErrorId INVALID_OLD_PASSWORD = new ErrorId("E0018", "Old password is required.");

	public static final ErrorId INVALID_OTP = new ErrorId("E0022", "Invalid OTP");

	public static final ErrorId INVALID_PASSWORD_POLICY = new ErrorId("E0015", "No password policy found.");

	public static final ErrorId INVALID_REFERENCE = new ErrorId("E0003", "Invalid reference....!");

	public static final ErrorId INVALID_REGEX = new ErrorId("E0046", "Invalid column Regex");

	public static final ErrorId INVALID_ROLE = new ErrorId("E0008", "Invalid role");

	public static final ErrorId INVALID_SUBLIST = new ErrorId("E0031", "Invalid SubList");

	public static final ErrorId INVALID_TABLE_NAME = new ErrorId("E0038", "Invalid table name");

	/**
	 * The invalid user.
	 */
	public static final ErrorId INVALID_USER = new ErrorId("E0001", "Invalid user details.");

	public static final ErrorId INVALID_USER_EMAIL = new ErrorId("E0012", "Please provide valid mail");

	public static final ErrorId LIMIT_EXISTS_THIS_FIELD = new ErrorId("E0051", "Limit Exists For This Field Type...");

	public static final ErrorId LIST_NAME_EXITS = new ErrorId("E0049",
			"Sorry!!!...Table name is already exist. Please try with a different name. ");

	public static final ErrorId MANDATORY_REQUIRED = new ErrorId("E0055", "Mandatory Required");

	public static final ErrorId NO_RECORD_FOUND = new ErrorId("E0010", "No record found");

	public static final ErrorId NOT_A_VALID_REQUEST = new ErrorId("E0049", "Request not valid...");

	public static final ErrorId OLD_PASSWORD_NOT_MATCHED = new ErrorId("E0019", "Invalid Old password.");

	public static final ErrorId OPTIONS_LIST_NULL = new ErrorId("E0044", "Field Options List Is Null...");

	public static final ErrorId OTP_EXPIRED = new ErrorId("E0012", "OTP Expired");

	public static final ErrorId PASSWORD_NOT_MATCHED = new ErrorId("E0016",
			"New password and Confirm password not matched.");

	public static final ErrorId REFERENCE_ERROR = new ErrorId("E0033", "Please fill the valid reference");

	public static final ErrorId ROLE_LIMIT_REACHED = new ErrorId("E0024", "Role limit reached");

	public static final ErrorId ROLE_MAPPED_USER = new ErrorId("E0007",
			"cannot delete the selected role as it is already mapped to user..");

	public static final ErrorId SUB_LIST_NOT_AVAILABLE = new ErrorId("E0036", "SubList Not Available");

	public static final ErrorId USER_ACC_INACTIVE = new ErrorId("E0028", "Your account is Inactive...");

	public static final ErrorId USER_BLOCKED = new ErrorId("E0024",
			"This user is blocked please try after few mins...");

	public static final ErrorId USER_LIMIT_REACHED = new ErrorId("E0025", "User limit reached");

	public static final ErrorId USER_NOT_FOUND = new ErrorId("E0009", "User not found");

	private ErrorCodes() {
	}
}

	
