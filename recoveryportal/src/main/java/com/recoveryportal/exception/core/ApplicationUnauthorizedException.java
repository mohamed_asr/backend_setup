package com.recoveryportal.exception.core;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.recoveryportal.exception.core.codes.ErrorId;
import com.recoveryportal.exception.core.codes.ErrorIdList;

/**
 * The Class ApplicationUnauthorizedException.
 */
@ResponseStatus(value= HttpStatus.UNAUTHORIZED)
public class ApplicationUnauthorizedException extends ApplicationException{
		
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 674090085986146600L;

	/**
	 * Instantiates a new application unauthorized exception.
	 *
	 * @param errorId    the error id
	 * @param httpStatus the http status
	 */
	public ApplicationUnauthorizedException(ErrorId errorId, HttpStatus httpStatus) {
		super(new ErrorIdList(errorId).convertToJsonString());
	}
}
