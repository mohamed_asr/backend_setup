package com.recoveryportal.exception.core.applicationutils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class ApplicationExceptionUtils {

	/**
	 * Checks if is blank.
	 *
	 * @param input the input
	 * @return true, if is blank
	 */
	public static boolean isBlank(String input) {
		return input == null || input.trim().isEmpty();
	}

	/**
	 * Checks if is not blank.
	 *
	 * @param input the input
	 * @return true, if is not blank
	 */
	public static boolean isNotBlank(String input) {
		return !isBlank(input);
	}

	/**
	 * Checks if is validate object.
	 *
	 * @param obj the obj
	 * @return true, if is validate object
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isValidateObject(Object obj) {
		if ((obj == null) || (obj instanceof List<?> && ((Collection<?>) obj).isEmpty())) {
			return false;
		}
		if (obj instanceof Map<?, ?> && ((Map) obj).isEmpty()) {
			return false;
		}

		if (obj instanceof Set<?>) {
			return !((Set) obj).isEmpty();
		}

		return true;
	}

	private ApplicationExceptionUtils() {
		/**/}
}
