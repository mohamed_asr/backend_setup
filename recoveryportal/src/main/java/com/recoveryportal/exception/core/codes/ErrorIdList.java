package com.recoveryportal.exception.core.codes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class ErrorIdList.
 */
public class ErrorIdList implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The error ids.
	 */
	private List<ErrorId> errorIds = new ArrayList<>();

	/**
	 * Instantiates a new error id list.
	 */
	public ErrorIdList() {
	}

	/**
	 * Instantiates a new error id list.
	 *
	 * @param errorId the error id
	 */
	public ErrorIdList(ErrorId errorId) {
		this.errorIds.add(errorId);
	}

	/**
	 * Instantiates a new error id list.
	 *
	 * @param errorList the error list
	 */
	public ErrorIdList(List<ErrorId> errorList) {
		this.errorIds = errorList;
	}

	/**
	 * Adds the error.
	 *
	 * @param errorId the error id
	 */
	public void addError(ErrorId errorId) {
		this.errorIds.add(errorId);
	}

	/**
	 * Convert to json string.
	 *
	 * @return the string
	 */
	public String convertToJsonString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();

		}
		return null;
	}

	/**
	 * Gets the error ids.
	 *
	 * @return the error ids
	 */
	public List<ErrorId> getErrorIds() {
		return this.errorIds;
	}

	/**
	 * Sets the error list.
	 *
	 * @param errorList the new error list
	 */
	public void setErrorList(List<ErrorId> errorList) {
		this.errorIds = errorList;
	}
	

}
