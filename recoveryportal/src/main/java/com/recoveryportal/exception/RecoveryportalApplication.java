package com.recoveryportal.exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecoveryportalApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecoveryportalApplication.class, args);
	}

}
