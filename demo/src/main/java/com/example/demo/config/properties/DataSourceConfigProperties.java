package com.example.demo.config.properties;

import java.beans.PropertyVetoException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableTransactionManagement
public class DataSourceConfigProperties {
	
	private static final Logger logger = LoggerFactory.getLogger(DataSourceConfigProperties.class);
	
	private final ComboPooledDataSource masterDataSource = new ComboPooledDataSource();
	
	@Autowired
	@Bean(name = "masterDataSource")
	@Primary
	public DataSource getMasterDataSource(DatabaseProperties databaseProperties) {
		try {
			masterDataSource.setDriverClass(databaseProperties.getJdbcDriver());
			masterDataSource.setJdbcUrl(databaseProperties.getJdbcUrl());
			masterDataSource.setUser(databaseProperties.getJdbcUser());
			masterDataSource.setPassword(databaseProperties.getJdbcPassword());

			logger.info(databaseProperties.getJdbcUser());
			logger.info(databaseProperties.getJdbcPassword());
		} catch (PropertyVetoException e) {
			logger.error("Can't Set Data Source for Master %s", e);
		}

		return masterDataSource;
	}
	
	

}
