package com.example.demo.config.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.example.demo.constants.core.PropertyConstants;

@Configuration
public class DatabaseProperties {
	
	private String dataBaseName = "";
	
	@Autowired
	private EnvironmentProperties environmentProperties;
	
	public String getJdbcDriver() {
		return environmentProperties.getJdbcDriver();
	}
	
	public String getJdbcPassword() {
		return environmentProperties.getJdbcPassword();
	}
	
	public String getJdbcUrl() {
		String url = replaceDateBaseName(environmentProperties.getDataSourceUrl());
		url = url.replace(PropertyConstants.MAIN_APP_MYSQL_IP, environmentProperties.getSqlIp());
		url = url.replace(PropertyConstants.MAIN_APP_MYSQL_PORT, environmentProperties.getSqlPort());
		return url;
	}
	
	public String getJdbcUser() {
		return environmentProperties.getJdbcUser();
	}
	
	public String getMySqlDateBase() {
		if (dataBaseName.isEmpty()) {
			dataBaseName = environmentProperties.getMySqlDateBase();
		}
		return dataBaseName;
	}
	
	public String replaceDateBaseName(String data) {
		data = data.replace(PropertyConstants.DB_NAME, getMySqlDateBase());
		return data;
	}

}
