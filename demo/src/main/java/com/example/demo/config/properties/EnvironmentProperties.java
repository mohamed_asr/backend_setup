package com.example.demo.config.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.example.demo.constants.core.PropertyConstants;

@Configuration
@PropertySource("classpath:/environment.properties")
@PropertySource(value = "file:///${ENVIRONMENT_PROPERTIES}/environment.properties", ignoreResourceNotFound = true)
public class EnvironmentProperties {
	
	private String dataBaseName = "";
	
	@Autowired
	private Environment environment;
	
	
	public String getDataSourceUrl() {
		return environment.getProperty(PropertyConstants.MYSQL_DATASOURCE_URL);
	}
	
	
	public String getDriverName() {
		return environment.getProperty(PropertyConstants.MYSQL_DRIVER_NAME);
	}
	
	public String getJdbcDriver() {
		return environment.getProperty(PropertyConstants.MYSQL_DRIVER_NAME);
	}
	
	
	public String getJdbcPassword() {
		return environment.getProperty(PropertyConstants.MYSQL_PASSWORD);
	}
	
	
	public String getMySqlDateBase() {
		if (dataBaseName.isEmpty()) {
			dataBaseName = environment.getProperty(PropertyConstants.MYSQL_DATA_BASE);
		}
		return dataBaseName;
	}
	
	
	public String getJdbcUrl() {
		String url = replaceDateBaseName(environment.getProperty(PropertyConstants.MYSQL_DATASOURCE_URL));
		url = url.replace(PropertyConstants.MAIN_APP_MYSQL_IP,
				environment.getProperty(PropertyConstants.MYSQL_IP_ADDRESS));
		url = url.replace(PropertyConstants.MAIN_APP_MYSQL_PORT,
				environment.getProperty(PropertyConstants.MYSQL_PORT_NUMBER));
		return url;
	}
	
	
	public String getJdbcUser() {
		return environment.getProperty(PropertyConstants.MYSQL_USER_NAME);
	}

	
      public String replaceDateBaseName(String data) {
		
        data = data.replace(PropertyConstants.DB_NAME, getMySqlDateBase());
  		return data;
	}

      public String getSqlIp() {
  		return environment.getProperty(PropertyConstants.MYSQL_IP_ADDRESS);
  	}

  	public String getSqlPort() {
  		return environment.getProperty(PropertyConstants.MYSQL_PORT_NUMBER);
  	}
  	
}
