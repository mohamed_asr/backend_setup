package com.recoveryportal.utils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecoveryportalUtilsDevelopApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecoveryportalUtilsDevelopApplication.class, args);
	}

}
