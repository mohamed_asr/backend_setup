package com.recoveryportal.utils;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.recoveryportal.exception.core.ApplicationException;
import com.recoveryportal.exception.core.codes.ErrorId;
import com.recoveryportal.utils.core.ApplicationUtils;

@Component
public class CommonValidatorUtils {
	
	 public static void validateStatus(Boolean value, ErrorId errorId) throws ApplicationException {
	        if (Boolean.TRUE.equals(value) || Boolean.TRUE.equals(!value)) {
	            throw new ApplicationException(errorId);
	        }
	    }
	    
	    
	    private CommonValidatorUtils() {

	    	
	    }

	    
	    public static void validateId(Integer id, ErrorId errorId) throws ApplicationException {
	        if (id == null || id < 1) {
	            getLogger().error("Invalid Id :: {}", id);
	            throw new ApplicationException(errorId);
	        }
	    }

	    
	    public static void validateDouble(Double value, ErrorId errorId) throws ApplicationException {
	        if (value == null || value < 0) {
	            getLogger().error("Invalid Id :: {}", value);
	            throw new ApplicationException(errorId);
	        }
	    }

	    
	    public static void validateString(String value, ErrorId errorId) throws ApplicationException {
	        if (ApplicationUtils.isBlank(value)) {
	            throw new ApplicationException(errorId);
	        }

	        if (value.equals("NaN")) {
	            throw new ApplicationException(errorId);
	        }
	    }



	    public static void validateIdentity(String value, ErrorId errorId) throws ApplicationException {
	        if (!ApplicationUtils.isValidIdentity(value)) {
	            throw new ApplicationException(errorId);
	        }
	    }

	    
	    public static boolean isValidId(Integer id) {
	        boolean expression = false;
	        if (id == null || id < 1) {
	            return expression;
	        }
	        expression = true;
	        return expression;
	    }

	    public static boolean isValidString(String value) {
	        boolean expression = false;
	        if (ApplicationUtils.isBlank(value)) {
	            return expression;
	        }
	        expression = true;
	        return expression;
	    }

	    
	    public static void validateObject(Object obj, ErrorId errorId) throws ApplicationException {
	        if (obj == null) {
	            throw new ApplicationException(errorId);
	        }
	        if (obj instanceof List<?> && ((Collection<?>) obj).isEmpty()) {
	            throw new ApplicationException(errorId);
	        }
	        if (obj instanceof Map<?, ?> && ((Map<?, ?>) obj).isEmpty()) {
	            throw new ApplicationException(errorId);
	        }
	    }

	    public static void validateList(List<?> list, ErrorId errorId)
	            throws ApplicationException {
	        if (ApplicationUtils.isValidateObject(list) && ApplicationUtils.isValidateObject(errorId)) {
	            throw new ApplicationException(errorId);
	        }
	    }

	    private static Logger getLogger() {
	        return LoggerFactory.getLogger(CommonValidatorUtils.class);
	    }

	    public static void validateDate(Date obj, ErrorId errorId) throws ApplicationException {
	        if (obj == null) {
	            throw new ApplicationException(errorId);
	        }
	    }

	    public static boolean isValidList(List<?> list) {
	        boolean expression = false;
	        if (list == null || list.isEmpty()) {
	            return expression;
	        }
	        expression = true;
	        return expression;

	    }

	    public static boolean isValidateObject(Object obj) {
	        boolean expression = false;
	        if (obj == null) {
	            return expression;
	        }
	        expression = true;
	        return expression;
	    }


}
