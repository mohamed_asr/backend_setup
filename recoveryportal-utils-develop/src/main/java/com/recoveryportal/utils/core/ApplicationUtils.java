package com.recoveryportal.utils.core;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * The Class ApplicationUtils.
 */
@Component
public class ApplicationUtils {
	
	private static final String RANGE = "0123456789";

	/**
	 * Gets the unique id.
	 *
	 * @return the unique id
	 */
	public static String getUniqueId() {
		return UUID.randomUUID().toString();
	}

	/**
	 * Checks if is blank.
	 *
	 * @param input the input
	 * @return true, if is blank
	 */
	public static boolean isBlank(String input) {
		return input == null || input.trim().isEmpty();
	}

	/**
	 * Checks if is not blank.
	 *
	 * @param input the input
	 * @return true, if is not blank
	 */
	public static boolean isNotBlank(String input) {
		return !isBlank(input);
	}

	/**
	 * Checks if is not valid id.
	 *
	 * @param id the id
	 * @return true, if is not valid id
	 */
	public static boolean isNotValidId(Integer id) {
		return !isValidId(id);
	}

	/**
	 * Checks if is validate object.
	 *
	 * @param obj the obj
	 * @return true, if is validate object
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isValidateObject(Object obj) {
		if (obj == null) {
			return false;
		}

		if (obj instanceof List<?> && ((Collection<?>) obj).isEmpty()) {
			return false;
		}

		if (obj instanceof Map<?, ?> && ((Map) obj).isEmpty()) {
			return false;
		}

		if (obj instanceof Set<?>) {
			return !((Set) obj).isEmpty();
		}

		return true;
	}

	public static boolean isValidString(String value) {
		return value != null;
	}

	/**
	 * Checks if is valid id.
	 *
	 * @param id the id
	 * @return true, if is valid id
	 */
	public static boolean isValidId(Integer id) {
		return id != null && id >= 1;
	}

	/**
	 * Checks if is valid identity.
	 *
	 * @param identity the identity
	 * @return true, if is valid identity
	 */
	public static boolean isValidIdentity(String identity) {
		return !isBlank(identity) && !identity.trim().equals("-1") && !identity.equalsIgnoreCase("undefined")
				&& !identity.equalsIgnoreCase("null");
	}

	/**
	 * Checks if is valid list.
	 *
	 * @param list the list
	 * @return true, if is valid list
	 */
	public static boolean isValidList(List<?> list) {
		return list != null && !list.isEmpty();

	}

	/**
	 * Checks if is valid long.
	 *
	 * @param value the value
	 * @return true, if is valid long
	 */
	public static boolean isValidLong(Long value) {
		return value != null && value >= 0;
	}



	public static boolean isBcryptPasswordMatched(String password, String encryptedPasssword) {
		return new BCryptPasswordEncoder().matches(password, encryptedPasssword);
	}

	public static boolean isValidObject(Object input) {
		return (input != null);
	}

	/**
	 * To Generate otp
	 *
	 * @throws
	 */
	public String generateOtp(Integer otpLength) {
		String numbers = RANGE;
		char[] otp = new char[otpLength];
		for (int i = 0; i < otpLength; i++) {
			otp[i] = numbers.charAt(ThreadLocalRandom.current().nextInt(numbers.length()));
		}
		return new String(otp);
	}


}
