package com.recoveryportal.utils.core;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * The Class ApplicationDataUtils.
 */
@Component
public class ApplicationDateUtils {
	
	/**
     * The Constant DATE_FORMAT.
     */
    public static final String DATE_FORMAT = "dd-MM-yyyy HH:mm";
    /**
     * The Constant EST_TIMEZONE.
     */
    public static final String EST_TIMEZONE = "EST";
    /**
     * The Constant QUERY_SEARCH_DATE_FORMAT.
     */
    public static final String QUERY_SEARCH_DATE_FORMAT = "dd-MM-yyyy HH:mm";
    /**
     * The Constant UTC_TIMEZONE.
     */
    public static final String UTC_TIMEZONE = "UTC";
    private static final int MIN = 60;
    private static final int MINUS_ONE = -1;
    /**
     * The Constant ZERO.
     */
    private static final int ZERO = 0;
    /**
     * The Constant logger.
     */
    private static final Logger logger = LoggerFactory.getLogger(ApplicationDateUtils.class);
    /**
     * The Constant timeZone.
     */
    private static final Map<String, String> timeZone = new HashMap<>();
    /**
     * The Milliseconds value.
     */
    private static final int MILLISECS = 1000;


    /**
     * Convert date with custom format.
     *
     * @param fromFromat the from fromat
     * @param toFormat   the to format
     * @param date       the date
     * @return the string
     */
    public static String convertDateWithCustomFormat(String fromFromat, String toFormat, String date) {
        DateFormat originalFormat = new SimpleDateFormat(fromFromat, Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat(toFormat);
        Date dates = new Date();
        try {
            dates = originalFormat.parse(date);
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }
        return targetFormat.format(dates);
    }

    /**
     * Convert date with time zone and compare.
     *
     * @param timeZone the time zone
     * @param date     the date
     * @param today    the today
     * @return true, if successful
     */
    public static boolean convertDateWithTimeZoneAndCompare(String timeZone, Date date, Date today) {

        ZoneId zoneId = ZoneId.of(getCoventionalTimeZone(timeZone));
        LocalDateTime zonedDateTime = LocalDateTime.ofInstant(date.toInstant(), zoneId);
        ZonedDateTime clientTime = zonedDateTime.atZone(zoneId);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(today.toInstant(), zoneId);
        ZonedDateTime serverTime = localDateTime.atZone(zoneId);
        if (clientTime.getMonthValue() < serverTime.getMonthValue()) {
            return true;
        }
        if (clientTime.getMonthValue() == serverTime.getMonthValue()) {
            return clientTime.getDayOfMonth() < serverTime.getDayOfMonth();
        }
        return false;

    }

    /**
     * Convert one format to another format.
     *
     * @param dateString the date string
     * @param fromFormat the from format
     * @param toFormat   the to format
     * @return the string
     */
    public static String convertOneFormatToAnotherFormat(String dateString, String fromFormat, String toFormat) {

        DateFormat sdf = new SimpleDateFormat(fromFormat);
        Date date = null;
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new SimpleDateFormat(toFormat).format(date);

    }

    /**
     * Gets the coventional time zone.
     *
     * @param timeZoneStr the time zone str
     * @return the coventional time zone
     */
    public static String getCoventionalTimeZone(String timeZoneStr) {
        return timeZone.get(timeZoneStr) == null ? timeZoneStr : timeZone.get(timeZoneStr);
    }

    /**
     * Gets the date 12 hrs after.
     *
     * @param date the date
     * @return the date 12 hrs after
     */
    public static Date getDate12HrsAfter(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, +12);
        return cal.getTime();
    }

    /**
     * Gets the file date.
     *
     * @return the file date
     */
    public static String getFileDate() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR) + "_" + (cal.get(Calendar.MONTH) + 1) + "_" + cal.get(Calendar.DAY_OF_MONTH) + "_"
                + cal.get(Calendar.HOUR_OF_DAY) + "_" + cal.get(Calendar.MINUTE) + "_" + cal.get(Calendar.SECOND);
    }

    /**
     * Gets the str date from date.
     *
     * @param dateFormat the date format
     * @param date       the date
     * @return the str date from date
     */
    public static String getStrDateFromDate(String dateFormat, Date date) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        if (date == null) {
            return null;
        }
        return format.format(date);
    }

    /**
     * Gets the XML gregorian calendar.
     *
     * @param date the date
     * @return the XML gregorian calendar
     */
    public static XMLGregorianCalendar getXMLGregorianCalendar(Date date) {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        XMLGregorianCalendar date2 = null;
        try {
            date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        } catch (DatatypeConfigurationException e) {
            logger.error("Error while converting date to XMLGregorianCalendar ".concat(e.getMessage()) );
            e.printStackTrace();
        }
        return date2;
    }

    /**
     * Gets the year end.
     *
     * @return the year end
     */
    public static Date getYearEnd() {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));

        return calendar.getTime();
    }

    /**
     * Gets the year start.
     *
     * @return the year start
     */
    public static Date getYearStart() {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar.getTime();
    }

    /**
     * Adds the one minute.
     *
     * @param date the date
     * @return the date
     */
    public Date addOneMinute(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, 1);
        return cal.getTime();
    }

    /**
     * Builds the calendar.
     *
     * @param date the date
     * @param zone the zone
     * @return the calendar
     */
    public Calendar buildCalendar(Date date, String zone) {
        Calendar calendarTest = new GregorianCalendar();
        calendarTest.setTime(date);

        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, calendarTest.get(Calendar.YEAR));
        calendar.set(Calendar.MONTH, calendarTest.get(Calendar.MONTH));
        calendar.set(Calendar.DATE, calendarTest.get(Calendar.DATE));
        calendar.set(Calendar.HOUR_OF_DAY, calendarTest.get(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendarTest.get(Calendar.MINUTE));

        if (zone != null) {
            TimeZone tz = TimeZone.getTimeZone(getCoventionalTimeZone(zone));
            calendar.setTimeZone(tz);
        }
        return calendar;
    }

    /**
     * Convert date diff by string.
     *
     * @param fromDate the from date
     * @param toDatte  the to datte
     * @return the integer
     * @throws ParseException the parse exception
     */
    public Integer convertDateDiffByString(String fromDate, String toDatte) throws ParseException {
        Date from = null;
        Date to = null;
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        from = format.parse(fromDate);
        to = format.parse(toDatte);
        return getDateDiff(from, to);
    }

    /**
     * Convert date with time zone.
     *
     * @param timeZone the time zone
     * @param date     the date
     * @return the date
     */
    public Date convertDateWithTimeZone(String timeZone, Date date) {
        ZoneId zoneId = ZoneId.of(getCoventionalTimeZone(timeZone));
        LocalDateTime zonedDateTime = LocalDateTime.ofInstant(date.toInstant(), zoneId);
        ZonedDateTime dateTime = zonedDateTime.atZone(zoneId);
        return java.util.Date.from(dateTime.toInstant());
    }

    /**
     * Convert est to UT cby adding hours based on daylight savings.
     *
     * @param date the date
     * @return the date
     */
    @SuppressWarnings("deprecation")
    public Date convertEstToUTCbyAddingHoursBasedOnDaylightSavings(Date date) {

        Integer month = date.getMonth();
        Integer day = date.getDate();

        // check the daylight saving and add 4 hours if else add 5 hours to est date

        if (month > 2 && month < 10 || month == 2 && day >= 8 || month == 10 && day == 1) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.HOUR_OF_DAY, 4);
            return calendar.getTime();
        }
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, 5);
		return calendar.getTime();
    }

    /**
     * Convert str into date.
     *
     * @param date       the date
     * @param dateFormat the date format
     * @param timeZone   the time zone
     * @return the date
     */
    public Date convertStrIntoDate(String date, String dateFormat, String timeZone) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(TimeZone.getTimeZone(getCoventionalTimeZone(timeZone)));

        Date dateObj = null;
        try {
            dateObj = sdf.parse(date);
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }
        return dateObj;
    }

    /**
     * Gets the date diff.
     *
     * @param fromDate the from date
     * @param toDatte  the to datte
     * @return the date diff
     */
    public Integer getDateDiff(Date fromDate, Date toDatte) {
        if (fromDate != null && toDatte != null) {
            long diff = fromDate.getTime() - toDatte.getTime();
            int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            return Math.abs(days);
        }
        return 0;
    }

    /**
     * Gets the date with end time.
     *
     * @param date the date
     * @return the date with end time
     */
    public Date getDateWithEndTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    /**
     * Gets the date with zero.
     *
     * @param date the date
     * @return the date with zero
     */
    public Date getDateWithZero(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * Gets the first date of month.
     *
     * @param date the date
     * @return the first date of month
     */
    public Date getFirstDateOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * Gets the first day of the month of the year.
     *
     * @param year  the year
     * @param month the month
     * @return the first day of the month of the year
     */
    public Date getFirstDayOfTheMonthOfTheYear(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * Gets the last day of the month of the year.
     *
     * @param year  the year
     * @param month the month
     * @return the last day of the month of the year
     */
    public Date getLastDayOfTheMonthOfTheYear(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    /**
     * Gets the next day.
     *
     * @param cal the cal
     * @return the next day
     */
    public void getNextDay(Calendar cal) {
        cal.add(Calendar.DATE, 1);
    }

    /**
     * Gets the next hour.
     *
     * @return the next hour
     */
    public Calendar getNextHour() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, 1);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    /**
     * Gets the next hour.
     *
     * @param cal the cal
     * @return the next hour
     */
    public Calendar getNextHour(Calendar cal) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(cal.getTime());
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        return calendar;
    }

    /**
     * Gets the next month.
     *
     * @param cal the cal
     * @return the next month
     */
    public void getNextMonth(Calendar cal) {
        cal.add(Calendar.MONTH, 1);
    }

    /**
     * Gets the one day before.
     *
     * @param date the date
     * @return the one day before
     */
    public Date getoneDayBefore(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, MINUS_ONE);
        return calendar.getTime();
    }

    /**
     * Gets the server time zone.
     *
     * @return the server time zone
     */
    public String getServerTimeZone() {
        TimeZone tz = TimeZone.getDefault();
        return tz.getID();
    }

    /**
     * Gets the working days between two dates.
     *
     * @param fromDate the from date
     * @param toDate   the to date
     * @return the working days between two dates
     */
    /*
     * Calculates number of working days between the given date range
     */
    public Integer getWorkingDaysBetweenTwoDates(Date fromDate, Date toDate) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(fromDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(toDate);

        int workDays = 0;

        // Return 0 if start and end are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0;
        }

        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(toDate);
            endCal.setTime(fromDate);
        }

        do {
            // excluding start date
            startCal.add(Calendar.DAY_OF_MONTH, 1);
            if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
                    && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                ++workDays;
            }
        } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); // excluding end date

        return workDays;
    }

    /**
     * Gets the zoned date.
     *
     * @param timeZone the time zone
     * @param date     the date
     * @return the zoned date
     */
    public Date getZonedDate(String timeZone, String date) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
        Date dateObj = null;
        try {
            dateObj = df.parse(date);
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }
        return convertDateWithTimeZone(timeZone, dateObj);
    }

    /**
     * Checks if is future date.
     *
     * @param date the date
     * @return true, if is future date
     */
    public boolean isFutureDate(Date date) {
        Date currentDate = new Date();

        return date.compareTo(currentDate) > 0;
    }

    /**
     * Checks if is past date.
     *
     * @param date the date
     * @return true, if is past date
     */
    public boolean isPastDate(Date date) {
        Date currentDate = new Date();

        return date.compareTo(currentDate) < 0;
    }

    /**
     * Sets the max time.
     *
     * @param date the date
     * @return the date
     */
    public Date setMaxTime(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    /**
     * Sets the next day max time.
     *
     * @param date the date
     * @return the date
     */
    public Date setNextDayMaxTime(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    /**
     * Sets the next day zero time.
     *
     * @param date the date
     * @return the date
     */
    public Date setNextDayZeroTime(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar.getTime();
    }

    /**
     * Sets the zero time.
     *
     * @param date the date
     * @return the date
     */
    public Date setZeroTime(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar.getTime();
    }

    /**
     * Simple time zone converter.
     *
     * @param clientZoneName the client zone name
     * @return the date
     */
    public Date simpleTimeZoneConvertor(String clientZoneName) {
        return convertDateWithTimeZone(clientZoneName, new Date());
    }


    /**
     * Gets the date difference.
     *
     * @param fromDate the from date
     * @param toDatte  the to date
     * @return the date difference
     */
    public Integer getDateDifference(Date fromDate, Date toDatte) {
        if (fromDate != null && toDatte != null) {
            long diff = (fromDate.getTime() - toDatte.getTime()) > ZERO ? (fromDate.getTime() - toDatte.getTime()) : MINUS_ONE;
            return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        }
        return 0;
    }

    /**
     * To validate Exp time
     *
     * @param expiryDate
     * @return boolean
     */
    public boolean isExpired(Date expiryDate) {
        return expiryDate.getTime() < new Date().getTime();
    }

    /**
     * @param expiryDate
     * @return resent time
     */
    public Integer createResendOtpTime(Date expiryDate) {
        Integer resendTime = null;
        expiryDate = ApplicationUtils.isValidateObject(expiryDate) ? expiryDate : new Date();
        resendTime = (int) Math.abs(((new Date().getTime() - expiryDate.getTime()) / 1000));
        return resendTime;
    }

    /**
     * @param secs
     * @return time
     */
    public String convertToSecsOrMins(Integer secs) {
        Integer convertedTime = null;
        if (ApplicationUtils.isValidateObject(secs)) {
            convertedTime = secs / MIN;
            return (convertedTime > 0) ? convertedTime + "mins"
                    : secs + "secs";
        }
        return "few mins";
    }

    public Date createResendOtpDateFromConfigValue(Date generatedDate, String configValue) {
        long expirationTimeInMilliseconds = ApplicationUtils.isNotBlank(configValue) && ApplicationUtils.isValidateObject(generatedDate)
                ? generatedDate.getTime() + Long.parseLong(configValue) * MILLISECS
                : null;
        return ApplicationUtils.isValidateObject(expirationTimeInMilliseconds) ? new Date(expirationTimeInMilliseconds)
                : new Date();
    }

    public Date getResetExpiryTime(Integer expireTime) {
        long expirationTimeInMilliseconds = new Date().getTime() + (expireTime * 60 * MILLISECS);
        return ApplicationUtils.isValidateObject(expirationTimeInMilliseconds) ? new Date(expirationTimeInMilliseconds)
                : new Date();
    }

    public boolean isResetUrlValid(Date expiryDate) {
        return expiryDate.getTime() > new Date().getTime();
    }

    /**
     * @param type
     * @return
     */
    public Date createDateFromConfigValue(String expiryTime) {
        long expirationTimeInMilliseconds = ApplicationUtils.isNotBlank(expiryTime)
                ? new Date().getTime() + Long.parseLong(expiryTime) * MILLISECS
                : null;
        return ApplicationUtils.isValidateObject(expirationTimeInMilliseconds) ? new Date(expirationTimeInMilliseconds)
                : new Date();
    }


    public String convertToZonedDate(Date date, String timeZone, String dateFormat) {
        DateFormat df = new SimpleDateFormat(dateFormat);
        df.setTimeZone(TimeZone.getTimeZone(TimeZoneProvider.getConventionalTimeZone(timeZone)));
        return df.format(date);
    }


}
