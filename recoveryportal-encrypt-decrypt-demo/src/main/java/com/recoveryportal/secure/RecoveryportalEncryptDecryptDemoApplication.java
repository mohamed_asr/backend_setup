package com.recoveryportal.secure;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.recoveryportal.secure.code.Encryption;


@SpringBootApplication
@RestController
@RequestMapping("/check")
public class RecoveryportalEncryptDecryptDemoApplication {

	public static void main(String[] args)  {
		SpringApplication.run(RecoveryportalEncryptDecryptDemoApplication.class, args);
//		String answ=Encryption.doDecryption("Hello_World");
//		System.out.println(answ);
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Autowired
	private Encryption enc;
	
	@GetMapping("/{data}")
	public ResponseEntity<String> encrypting(@PathVariable String data) {
		try {
			String doEncryption = enc.doDecryption(data);
			return ResponseEntity.ok(doEncryption);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
}
