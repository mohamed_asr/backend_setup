package com.recoveryportal.secure.code;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class Encryption {
	@Autowired
	private static Environment env;
	
	
	@Value("${algorithm}")
	private String algorithm;
	
	@Value("${key.value}")
	private String securityKey;
	
	
	
	public String doEncryption(String data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		String secrKey=env.getProperty("key.value");
	    Key key= new SecretKeySpec(secrKey.getBytes(),"AES");
	    Cipher cf =Cipher.getInstance("AES");
	    cf.init(Cipher.ENCRYPT_MODE,key);
	    byte[] encryptedValue= new Base64().encode(data.getBytes());
	    return new String(encryptedValue);
	    
	}
	
	
	public String doDecryption(String data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		String secrKey=env.getProperty("key.value");
	    Key key= new SecretKeySpec(secrKey.getBytes(),"AES");
	    Cipher cf =Cipher.getInstance("AES");
	    cf.init(Cipher.DECRYPT_MODE,key);
	    byte[] decryptedValue= new Base64().decode(data.getBytes());
	    return new String(decryptedValue);
	    
	}
	
	
	

}
